FROM python:2.7.18-alpine3.11

WORKDIR /app

# Set environment variables
ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1

# Install system dependencies
RUN apk add --update --no-cache postgresql-client
RUN apk update && apk add python-dev \
        gcc libc-dev linux-headers postgresql-dev \
        libffi-dev openssl-dev cargo
RUN apk add musl-dev freetype libpng libjpeg-turbo freetype-dev libpng-dev libjpeg-turbo-dev

RUN pip install --upgrade pip
COPY requirements.txt /app/requirements.txt
RUN pip install -r requirements.txt


COPY . .

EXPOSE 8000

CMD ["python",  "manage.py",  "runserver",  "0.0.0.0:8000"]

