# Nautilux Test
Application to manage interventions done in python 2.7, Django 1.11, Postgresql 15.0, AngularJS 1.7.9, Bootstrap 5.4, Docker et docker-compose V2


## Installation
- Go inside the directory in which you want to install the application:
    - `git clone git@gitlab.com:Melissendra/nautilux_test.git`
    - `cd nautilux_test`

### Create your python environment:
- In a terminal: `openssl rand -hex 32`
- Create a .env file and add:
  `SECRET_KEY="secret_key" (copy/paste your terminal's code)
  POSTGRES_ENGINE="django.db.backends.postgresql"
  POSTGRES_DB="nautilux_db"
  POSTGRES_USER="test"
  POSTGRES_PASSWORD="test"
  POSTGRES_HOST=db
  POSTGRES_PORT=5432
  DEBUG=True`

### Application Setup
- docker compose (ou docker-compose for v1 of docker compose) up -d --build 
- `docker compose exec web python manage.py migrate`

If you want to use the django's admin system:
- `python manage.py createsuperuser`


## Endpoints
- Interface at this url: 
  - http://localhost:8080/#
- Endpoints api: 
  - Interventions list and creation of new one:
      - http://127.0.0.1:8000/interventions/
  - show details, update or delete of the selected intervention:
      - http://127.0.0.1:8000/interventions/manage_intervention/{id} => example: http://127.0.0.1:8000/interventions/manage_intervention/1/
  - Admin page: Here you manage the users' authentication if necessary for other superuser: http://127.0.0.1:8000/admin/

### How to use it:
- When on the interface, you could add an intervention by clicking the "Ajouter une intrevention" button.
  - ![](/static/application_screenshot.png)
- A modal will open:
  - ![](/static/add_intervention_modal_screenshot.png)
  - Here you'll have to write at least "libellé, description et le lieu" to create a new intervention. If not you'll have an alert dialog
  - By default, the intervention are display by intervention from the oldest to the most recent:
    - if you want to reverse order you can click on "Date de l'intervention"
    - You can also sort by status or operator_name (Status, Nom de l'intervenant)
  - The update button is not visible if the status is "Terminé"
  - On create an intervention, if you don't fill the status field, the api automatically updates the status:
    - if there's no operator_name and no intervention_date it set to "Brouillon"
    - else if the intervention date is in future date, status = "Validé" and if intervention date is oldest status = Terminé
    - else it's the field value that will be register.
  - I make that choice thinking it'll be easier for a user, less to setup. 
  - If an intervention status is "Terminé" user can't update: No button "Modifier" is displayed. 
  - To Update click on "Modifier": By default all the value are displayed only status is not. Here it's working exactly like create intervention
  - To delete just click on the line's cross