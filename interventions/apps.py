# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class InterventionsConfig(AppConfig):
    default_auto_field = 'django.db.models.fields.BigAutoField'
    name = 'interventions'
