from django.conf.urls import url
from interventions.views import interventions_list, manage_intervention

urlpatterns = [
    url(r'^$', interventions_list),
    url(r'^manage_intervention/(?P<pk>[0-9]+)/$', manage_intervention),
]
