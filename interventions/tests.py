# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIRequestFactory
from interventions.views import interventions_list, manage_intervention
from interventions.models import Intervention


# Create your tests here.
class TestIntervention(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.intervention = Intervention.objects.create(
            label='Test Intervention',
            description='Test Intervention Description',
            operator_name="",
            site="Test Site",
            intervention_date=None,
        )
        self.create_view = interventions_list
        self.manage_intervention = manage_intervention

    def test_create_intervention_valid_data(self):
        self.assertEqual(Intervention.objects.all().count(), 1)
        res = self.factory.post(
            'interventions/',
            {
                "label": "test connection",
                "description": "Pas d'internet depuis 3 jours",
                "operator_name": "Gaelle",
                "site": "Saintes",
                "intervention_date": "2022-10-22"
            }, format='json'
        )
        response = self.create_view(res)

        self.assertEqual(Intervention.objects.all().count(), 2)

    def test_get_intervention(self):
        intervention = Intervention(
            id=1,
            label="test connection",
            description="tests",
            operator_name="Gaelle",
            site="Saintes",
            intervention_date="2022-10-22"
        )
        intervention.save()
        res = self.factory.get('interventions/')
        response = self.manage_intervention(res, pk=1)
        response.render()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['label'], intervention.label)
        self.assertEqual(response.data['description'], intervention.description)
        self.assertEqual(response.data['operator_name'], intervention.operator_name)
        self.assertEqual(response.data['site'], intervention.site)
        self.assertEqual(response.data['intervention_date'], intervention.intervention_date)

