# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import datetime
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from interventions.models import Intervention
from interventions.serializers import InterventionSerializer


def check_operator_name_intervention_date(data):
    """
    Check if operator name and date are valid
    :param data:
    :return:
    """
    today = datetime.now()
    if data['intervention_date'] is None:
        data['status'] = "Brouillon"
    if data['operator_name'] and data['intervention_date'] is not None:
        intervention_date = datetime.strptime(data['intervention_date'], '%Y-%m-%d')
        data["status"] = "Validé" if intervention_date > today else "Terminé"
    return data


@api_view(['GET', 'POST'])
def interventions_list(request):
    """
    List all the interventions and create new one
    :param request: request to get or post
    :return: serializers data
    """
    data = request.data
    if request.method == 'GET':
        interventions = Intervention.objects.all().order_by('intervention_date')
        serializer = InterventionSerializer(interventions, many=True)
        return Response(serializer.data)
    elif request.method == 'POST':
        # checking if optional information are set and change status if so
        if 'intervention_date' in data and 'operator_name' in data and 'status' not in data:
            data = check_operator_name_intervention_date(data)
        serializer = InterventionSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def manage_intervention(request, pk):
    """
    Retrieve an intervention by its id
    :param request: Allow managing data from request
    :param pk: primary key id of teh intervention
    :return: json of the intervention
    """

    try:
        intervention = Intervention.objects.get(pk=pk)
    except Intervention.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        return Response(InterventionSerializer(intervention).data, status=status.HTTP_200_OK)
    elif request.method == 'PUT':
        data = check_operator_name_intervention_date(request.data)
        serializer = InterventionSerializer(intervention, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'DELETE':
        intervention.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

