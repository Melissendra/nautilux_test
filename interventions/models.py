# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


# Create your models here.
class Intervention(models.Model):
    """Intervention table"""
    label = models.CharField(max_length=255)
    description = models.CharField(max_length=255)
    operator_name = models.CharField(max_length=255, blank=True, null=True)
    site = models.CharField(max_length=255)
    intervention_date = models.DateField(null=True, blank=True)
    status = models.CharField(max_length=55, default="Brouillon")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.label
