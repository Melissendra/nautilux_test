Django==1.11.29
psycopg2==2.8.6
python-dotenv==0.18.0
pathlib==1.0.1
requests==2.27.1
djangorestframework==3.9.4
django-cors-headers==3.0.2