(function() {
    "use strict";
    // All the routes for managing the intervention
    angular
        .module("testNautilux")
        .factory("interventionsFactory", function ($http) {
            function getInterventionList() {
                return $http.get('http://localhost:8000/interventions/');
            }
            function addIntervention(data) {
                return $http.post('http://localhost:8000/interventions/', data);
            }
            function updateIntervention(data) {
                return $http.put('http://localhost:8000/interventions/manage_intervention/' + data.id +'/',  data);
            }
            function getInterventionById(data) {
                return $http.get('http://localhost:8000/interventions/manage_intervention/' + data.id +'/',  data);
            }
            function deleteIntervention(data) {
                return $http.delete('http://localhost:8000/interventions/manage_intervention/' + data.id +'/',  data);
            }
            return {
                getInterventionList: getInterventionList,
                getInterventionById:getInterventionById,
                addIntervention: addIntervention,
                updateIntervention: updateIntervention,
                deleteIntervention: deleteIntervention
            };

        });
})();