(function() {
    "use strict";
   // All the functions needed for displaying and managing interventions
    angular
        .module("testNautilux")
        .controller("TestNautiluxCtr", function($scope, $http, interventionsFactory) {
            // interventionsFactory.getInterventionList().then(function(response) {
            //     $scope.interventions = response.data;
            // });
            function getInterventionList() {
                interventionsFactory.getInterventionList().then(function(response) {
                    $scope.interventions = response.data;
                });
            }
            getInterventionList()
            $scope.addIntervention = function(intervention) {
                // Checking if all the required data are set
                if( (intervention)  && (intervention.label && intervention.description && intervention.site)) {
                    interventionsFactory.addIntervention(intervention).then(
                        function(){
                            getInterventionList()
                        }
                    );
                    $scope.intervention = {};
                } else {
                    alert("Please provide at least label, description and site")
                }
            };
            $scope.editIntervention = function(intervention) {
                $scope.editing = true;
                $scope.intervention = intervention;
            };
            $scope.saveEdit = function(intervention) {
                interventionsFactory.updateIntervention( intervention).then(
                    function(){
                        getInterventionList();
                    }
                );
                $scope.intervention = {};
                $scope.editing = !$scope.editing;
            };
            $scope.cancelOp = function(){
                $scope.editing = !$scope.editing;
                $scope.intervention = {};
            }

            $scope.deleteIntervention = function(intervention) {
                interventionsFactory.deleteIntervention(intervention).then(
                    function(){
                        getInterventionList();
                    }
                );
            }
        });
})();